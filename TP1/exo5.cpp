#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    if (n==0) {
        return 0;
    }
    else {
        float module=z.length();
        if (module>2){
            return n;
        }
        else {
            float nouv_x=z.x*z.x-z.y*z.y+point.x;
            float nouv_y=2*z.x*z.y+point.y;
            z.x=nouv_x;
            z.y=nouv_y;
            return isMandelbrot(z, n-1, point);
        }
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



