#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
};

struct DynaTableau{
    int* donnees;
};

//J'ai tout codé mais quand j'essaye d'exécuter il ne se passe rien..

void initialise(Liste* liste)
{
    if(!liste)
    {
        exit(EXIT_FAILURE);
    }
    liste->premier=NULL;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier==NULL){
        return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* new_valeur= (Noeud*) malloc(sizeof(*new_valeur));
    if (new_valeur == 0) {
            cout << "Memory has run out " << endl;
            exit(1);
    }
    new_valeur->donnee = valeur;
    new_valeur->suivant = NULL;
    Noeud* prov= liste->premier;
    while (prov->suivant!=NULL){
        prov=prov->suivant;
    }
    prov->suivant=new_valeur;
}

void affiche(const Liste* liste)
{
    Noeud* current=liste->premier;
    while(current!=NULL) {
        cout << current->donnee << endl;
        current=current->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    int i=0;
    Noeud* current=liste->premier;
    while (i!=n){
        if (current!=NULL){
            current=current->suivant;
            i++;
        }
        else return 0;
    }
    return current->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    int i=0;
    Noeud* current=liste->premier;
    while (current!=NULL){
        if (current->donnee==valeur){
            return i;
        }
        else {
            current=current->suivant;
            i++;
        }
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* current=liste->premier;
    int i=0;
    while (i!=n){
        if (current!=NULL){
            current=current->suivant;
            i++;
        }
        else {
            cout << "No value at index n" << endl;
            exit(1);
        }
    }
    current->donnee=valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    int i=0;
    while (tableau->donnees[i]!=NULL){
        i++;
    }
    tableau->donnees[i]=valeur;
}


void initialise(DynaTableau* tableau, int capacite)
{
    if(!tableau){
        exit(EXIT_FAILURE);
    }
    for(int i=0; i<capacite; i++){
        tableau->donnees[i]=NULL;
    }
}

bool est_vide(const DynaTableau* liste)
{
    if (liste->donnees==NULL){
        return true;
    }
    return false;
}

void affiche(const DynaTableau* tableau)
{
    int i=0;
    while (tableau->donnees[i]!=NULL){
        cout << tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if (tableau->donnees[n]!=NULL){
        return tableau->donnees[n];
    }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int i=0;
    while(tableau->donnees[i]!=NULL){
        if (tableau->donnees[i]==valeur){
            return i;
        }
        i++;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n]=valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud* new_valeur= (Noeud*) malloc(sizeof(*new_valeur));
    if (new_valeur == 0) {
            cout << "Memory has run out " << endl;
            exit(1);
    }
    new_valeur->donnee = valeur;
    new_valeur->suivant = NULL;
    Noeud* prov= liste->premier;
    while (prov->suivant!=NULL){
        prov=prov->suivant;
    }
    prov->suivant=new_valeur;
}

//int retire_file(DynaTableau* liste)
int retire_file(Liste* liste)
{
    if (!est_vide(liste)){
        Noeud *prem=liste->premier;
        int res;
        res=prem->donnee;
        prem=prem->suivant;
        return res;
    }
    return 0;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(DynaTableau* liste, int valeur)
{
    int i=0;
    while (liste->donnees[i]!=NULL){
        i++;
    }
    liste->donnees[i]=valeur;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(DynaTableau* liste)
{
    if (!est_vide(liste)){
        int i=0;
        int res;
        while(liste->donnees[i]!=NULL){
            i++;
        }
        res=liste->donnees[i-1];
        liste->donnees[i-1]=NULL;
        return res;
    }
    return 0;
}

int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    DynaTableau pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile, 7);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
