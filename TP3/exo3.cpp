#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    BinarySearchTree* left;
    BinarySearchTree* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left= this->right = NULL;
        this->value=value;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        BinarySearchTree *nouv = new BinarySearchTree(value);
        nouv->initNode(value);
        if (value<this->value){
            if (this->left==NULL){
                (this->left)=nouv;
            }
            else (this->left)->insertNumber(value);
        }
        else {
            if (this->right==NULL){
                (this->right)=nouv;
            }
            else (this->right)->insertNumber(value);
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->left==NULL && this->right==NULL){
            return 1;
        }
        else {
            uint taille_g=0;
            uint taille_d=0;
            if (this->left){
                taille_g=(this->left)->height();
            }
            if (this->right){
                taille_d=(this->right)->height();
            }
            if (taille_g>taille_d){
                return taille_g+1;
            }
            else return taille_d+1;
        }
    }

    uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->left==NULL && this->right==NULL){
            return 1;
        }
        else if(this->left==NULL){
            return (this->right)->nodesCount()+1;
        }
        else if(this->right==NULL){
            return (this->left)->nodesCount()+1;
        }
        else return (this->left)->nodesCount()+(this->right)->nodesCount()+1;
    }

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if (this->left==NULL && this->right==NULL){
            return true;
        }
        return false;
	}

    void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(this->isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        }
        else {
            if (this->left){
                (this->left)->allLeaves(leaves, leavesCount);
            }
            if(this->right){
                (this->right)->allLeaves(leaves, leavesCount);
            }
        }
    }

    void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if (this->left){
            (this->left)->inorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount]=this;
        nodesCount++;
        if (this->right){
            (this->right)->inorderTravel(nodes, nodesCount);
        }
    }

    void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount]=this;
        nodesCount++;
        if (this->left){
            (this->left)->preorderTravel(nodes, nodesCount);
        }
        if (this->right){
            (this->right)->preorderTravel(nodes, nodesCount);
        }
}

    void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if (this->left){
            (this->left)->postorderTravel(nodes, nodesCount);
        }
        if (this->right){
            (this->right)->postorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount]=this;
        nodesCount++;
    }

    BinarySearchTree* find(int value) {
        // find the node containing value
        BinarySearchTree * noeud = this;
        while(noeud){
            if(noeud->value==value){
                return noeud;
            }
            if(noeud->value>value){
                if(noeud->left){
                    noeud=noeud->left;
                }
                else return nullptr;
            }
            if(noeud->value<value){
                if(noeud->right){
                    noeud=noeud->right;
                }
                else return nullptr;
            }
        }
        return nullptr;
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
